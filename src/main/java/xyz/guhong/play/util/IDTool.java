package xyz.guhong.play.util;

import java.util.UUID;

public class IDTool {
    /**
     * 数据库id字段
     *
     * @return
     */
    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }
}
