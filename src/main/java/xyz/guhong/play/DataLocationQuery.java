package xyz.guhong.play;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import xyz.guhong.play.core.DataLocationCore;

import java.util.Scanner;

/**
 * 查询行政区划信息
 *
 * @author 李双凯
 */
public class DataLocationQuery {
    /**
     * 主程序
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        JSONObject jsonObject = DataLocationCore.start();
        Scanner input = new Scanner(System.in);
        String code = "";
        do{
            System.out.println("你可以输入以下内容来查询行政区划信息：");
            System.out.println("all：查询全部 ， bye: 结束 ， 1：查询省级列表， 2：查询市级列表 ， 3：查询县级列表 ， 行政代码（如:北京 110000）");
            System.out.print("请输入:");
            code = input.next();
            System.out.println("=================================================================\n");
            if("all".equals(code)){
                DataLocationCore.showAll(jsonObject);
            }else if("bye".equals(code)){
                break;
            }else if("1".equals(code)){
                DataLocationCore.showProvince();
            }else if("2".equals(code)){
                DataLocationCore.showCity(jsonObject);
            }else if("3".equals(code)){
                DataLocationCore.showArea(jsonObject);
            }else{
                DataLocationCore.queryList(code,jsonObject);
            }
            System.out.println("\n=================================================================");
        }while (true);
        System.out.println("结束");
    }
}
