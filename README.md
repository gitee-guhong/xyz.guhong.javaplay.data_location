# xyz.guhong.javaplay.data_location

#### 介绍
中国行政区划数据
省市县三级sql、数据

#### 软件架构
中国行政区划数据。可以快速将省市县三级数据插入数据库中，并且提供一定的查询功能。
解决需要省市县数据的项目


#### 安装教程

1. 下载项目，加入到原本项目中，即可使用
2. 使用时可以启动DataLocationQuery类完成查询功能
3. 或者使用SaveToMySQL类把数据插入到mysql中。但需要先执行structure.sql文件

#### 使用说明

1. 核心类为DataLocationCore类
2. 如果你不想管程序。可以直接使用sql语句创建数据